#lang scribble/manual
@require[@for-label[rktermios
                    racket/base]
	 scribble/extract]

@title[#:version "0.1"]{@racketmodname[rktermios]}

@author[
@author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

@defmodule[rktermios]

This module allows the programmer to set the terminal to "raw" mode. It works
both under Racket BC and Racket CS.

Usage:

@racketblock[
(require rktermios)

(define tios-orig (rktermios-get))
(define tios-raw (rktermios-copy tios-orig))
(rktermios-make-raw! tios-raw)
(rktermios-load tios-raw)
(file-stream-buffer-mode (current-output-port) 'none)
...
(rktermios-load tios-orig)
]

@include-extracted["../main.rkt"]
